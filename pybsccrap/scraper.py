from pybsccrap.spiders import TokenSpider

from scrapy import signals
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from scrapy.signalmanager import dispatcher


def scrape_tokens():
    results = []

    def crawler_results(signal, sender, item, response, spider):
        results.append(item)

    dispatcher.connect(crawler_results, signal=signals.item_passed)
    settings = get_project_settings()
    process = CrawlerProcess(settings)
    process.crawl(TokenSpider)
    process.start()
    return results
