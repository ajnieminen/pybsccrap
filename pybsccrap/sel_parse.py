from time import sleep

from geckodriver_autoinstaller import install as gd_install
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait


def run_parse(address):
    page = 1
    page_count = 100
    next_page = True

    opts = webdriver.FirefoxOptions()
    opts.headless = True

    gd_install()
    driver = webdriver.Firefox(options=opts)
    wait = WebDriverWait(driver, 30)

    while next_page:

        url = 'https://bscscan.com/tokenholdings' \
              f'?a={address}' \
              f'&ps={page_count}' \
              f'&sort=total_price_usd' \
              f'&order=desc' \
              f'&p={page}'
        page += 1
        driver.get(url)
        nav = wait.until(
            ec.presence_of_element_located((By.CLASS_NAME, "pagination")))
        try:
            css_selector = "li[data-original-title='Go to Next']"
            nav.find_element(By.CSS_SELECTOR, css_selector)
        except NoSuchElementException:
            next_page = False

        table = wait.until(ec.presence_of_element_located((By.ID, 'tb1')))
        rows = []
        for i in range(10):
            sleep(1)  # TODO: get rid of this?
            rows = table.find_elements(By.TAG_NAME, 'tr')
            if len(rows) > 0:
                break

        for row in rows:
            cols = row.find_elements(By.TAG_NAME, 'td')
            # name = cols[1].text
            yield {
                'symbol': cols[2].text,  # TODO: LONG symbols are truncated
                'quantity': cols[3].text,
                't_price': cols[4].text,
                'val_bnb': cols[6].text,
                'val_usd': cols[7].text
            }

    driver.quit()
