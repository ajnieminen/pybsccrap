from scrapy import Spider


class TokenSpider(Spider):

    custom_settings = {
        'ROBOTSTXT_OBEY': False,
        'USER_AGENT': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:39.0) Gecko/20100101 Firefox/39.0'
    }

    name = 'matches'
    token = '0x69CC2956078369f00D5017250a90f94065440a77'
    baseurl = 'https://bscscan.com/tokenholdings'
    url = f'{baseurl}?a={token}&ps=25&sort=tokenname&order=asc'

    start_urls = [url]

    def _response_to_dict(self, date, match):
        return {
            'url': match.xpath('a/@href').extract()[0],
            'date': date,
            'time': match.css('div.time::text').get(),
            'teams': match.css('div.team::text').getall(),
            'event': match.css('span.event-name::text').get(),
            'map_text': match.css('div.map-text::text').get()
        }

    def parse(self, response):
        print('response', response.text)
        rows = response.css('tbody#tb1')[0].css('tr')
        print('xxx', rows)
