#!/usr/bin/env python
# coding: utf-8
"""pybsccrap

Usage:
  pybsccrap

Options:
  -h --help          Show this screen
"""
from docopt import docopt

from pybsccrap.sel_parse import run_parse


def run():
    arguments = docopt(__doc__)
    print('arguments', arguments)

    address = '0x69CC2956078369f00D5017250a90f94065440a77'
    for token in run_parse(address=address):
        print('token', token)
